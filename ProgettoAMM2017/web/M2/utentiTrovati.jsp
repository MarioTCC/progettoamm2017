<%-- 
    Document   : utentiTrovati
    Created on : 1-giu-2017, 16.39.01
    Author     : Mario Taccori
--%>

<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%@taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<json:array>
    <c:forEach var="utente" items="${listaUtentiTrovati}">
        <c:if test="${sessionScope.user != utente.id}">
            <json:object>
                <json:property name="nome" value="${utente.nome}"/>
                <json:property name="cognome" value="${utente.cognome}"/>
                <json:property name="id" value="${utente.id}"/>
                <json:property name="urlFotoProfilo" value="${utente.urlFotoProfilo}"/>
            </json:object>
        </c:if>
    </c:forEach>
</json:array>