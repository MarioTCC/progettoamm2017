<%-- 
    Document   : notifications
    Created on : 23-apr-2017, 18.43.06
    Author     : Mario Taccori
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true" %>

<c:choose>
    <c:when test="${notificationType == 'loginError' && notificationValue=='wrongCredentials'}">
        <div class="notificationDiv">
            <p>Nome utente e/o Password errati, riprovare.</p>
        </div>
    </c:when>
    
    <c:when test="${notificationType == 'loginError' && notificationValue=='emptyField'}">
        <div class="notificationDiv">
            <p>Per favore, compilare tutti i campi.</p>
        </div>
    </c:when>
    
    <c:when test="${notificationType == 'loginError' && notificationValue=='sessionExpired'}">
        <div class="notificationDiv">
            <p>Sei stato disconnesso.</p>
        </div>
    </c:when>
    
    <c:when test="${notificationType == 'accessDenied' && notificationValue=='accessDenied'}">
        <div class="notificationDiv">
            <p>Accesso negato. Per favore, effettuare il login.</p>
        </div>
    </c:when>
    
    <c:when test="${notificationType == 'userInfoIncompleteError' && notificationValue=='userInfoIncompleteError'}">
        <div class="notificationDiv">
            <p>I campi in rosso sono obbligatori</p>
        </div>
    </c:when>
    
    <c:when test="${notificationType == 'profileInfo' && notificationValue=='updatedProfileInfo' && campiModificati!='none'}">
        <div class="notificationDiv">
            <p>I seguenti campi sono stati aggiornati:</p>
            <ul class="notificationList">
                <c:forEach var="tmp" items="${campiModificati}">
                    <li>${tmp}</li>
                </c:forEach>
            </ul>
        </div>
    </c:when>
    
    <c:when test="${notificationType == 'accountDeletion' && notificationValue=='accountDeletionError'}">
        <div class="notificationDiv">
            <p>Si è verificato un errore, riprova.</p>
        </div>
    </c:when>
    
    <c:when test="${notificationType == 'newPostCreation' && notificationValue == 'newPostCreationOk'}">
        <div id="notificaNuovoPost">
            <p>Hai scritto sulla bacheca di ${owner.nome} <c:if test="${ownerType!='group'}">${owner.cognome}!</c:if></p>
        </div>
    </c:when>
    
    <c:when test="${notificationType == 'newPostCreation' && notificationValue == 'newPostCreationError'}">
        <div class="notificationDiv">
            <p>Si è verificato un errore, riprova.</p>
        </div>
    </c:when>
    
    <c:when test="${notificationType == 'passwordMissmatchError' && notificationValue == 'passwordMissmatch'}">
        <div class="notificationDiv">
            <p>Il campo password e conferma password non corrispondono, riprova.</p>
        </div>
    </c:when>
 
</c:choose>