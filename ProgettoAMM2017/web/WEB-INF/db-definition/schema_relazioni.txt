                +-------------------+                +----------------------+        +----------------------+
                |    Utenti         |                |      Posts           |        |       Tipi post      |
                +-------------------+                +----------------------+        +----------------------+
           +----+   id             +----+-----+      |    id                |   +-----+    id               |
           |    |   nome            |   |     +-------+   autore            |   |    |     nome tipo post   |
           |    |   cognome         |   |     |      |    contenuto         |   |    |                      |
           |    |   username        |   |     |      |    tipo post       +-----+    +----------------------+
           |    |   password        |   |     |      |    attached url      |
           |    |   email           |   |     |      |   tipo destinazione +-----+   +----------------------+
           |    |   data nascita    |   |     |      |    data post         |    |   |   Tipi destinazione  |
           |    |   urlfotoprofilo  |   |     +-------+   id utente dest    |    |   +----------------------+
           |    |   frasepresentaz  |   |       +-----+   id gruppo dest    |    +-----+   id               |
           |    |   tipo utente   +---+ |       |    |    ora post          |        |     nome tipo dest   |
           |    +-------------------+ | |       |    +----------------------+        |                      |
           |                          | |       |                                    +----------------------+
           |    +-------------------+ | |       |
           |    |     Tipi utente   | | |       |
           |    |-------------------| | |       |
           |    |    id        +------+ |       |
           |    | nome tipo utente  |   |       |
           |    |                   |   |       |
           |    +-------------------+   |       |
           |                            |       |
           |    +-------------------+   |       |
           |    |     Amici         |   |       |
           |    +-------------------+   |       |
           |    |    id utente1  +------+       |    +----------------------+
           |    |    id utente2  +------+       |    |      Gruppi          |
           |    |                   |   |       |    +----------------------+
           |    |                   |   |       +-------+   id              |
           |    +-------------------+   |       |    |      nome            |
           |                            |       |    |      url icona       |
           +-------------------------------------------+    fondatore       |
                +-------------------+   |       |    |                      |
                | Iscrizioni Gruppi |   |       |    |                      |
                +-------------------+   |       |    |                      |
                |     id utente    |----+       |    |                      |
                |     id gruppo    +------------+    |                      |
                |                   |                |                      |
                +-------------------+                +----------------------+

                

        Utenti è in relazione molti a molti con utenti (amici: un utente può essere amico di molti utenti, un utente può avere molti amici)
        Utenti è in relazione uno a molti con posts (un utente può scrivere molti post, un post ha un solo autore)
        Utenti è un relazione molti a molti con gruppi (un utente può essere iscritto a molti gruppi, a un gruppo possono essere iscritti molti utenti)
        posts è in relazione uno a molti con gruppi (un post appartiene a un solo gruppo, un gruppo può contenere molti post)
